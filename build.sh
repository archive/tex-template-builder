#!/usr/bin/env bash

set -e

SCRIPT=`realpath $0`
SCRIPTPATH=`dirname $SCRIPT`

ADDITIONAL_ARGS=${@:2}

INPUT_FILE=`realpath $1`
export INPUT_DIR=`dirname $INPUT_FILE`
METADATA_FILE="$INPUT_DIR/metadata.yaml"
export OUTPUT_FILE="$PWD/output.pdf"

INPUT_FILES="$INPUT_FILE"

if [ -f "$METADATA_FILE" ]
then
    INPUT_FILES="$INPUT_FILES $METADATA_FILE"
fi

PANDOC_ARGS="$INPUT_FILES --lua-filter=$SCRIPTPATH/variables.lua --template $SCRIPTPATH/main.tex --listings -o $OUTPUT_FILE $ADDITIONAL_ARGS"

echo "> Building document..."
cd $SCRIPTPATH && pandoc $PANDOC_ARGS

echo "> Running second pass..."
cd $SCRIPTPATH && pandoc --lua-filter=$SCRIPTPATH/word-count.lua $PANDOC_ARGS

echo "> Output to '$OUTPUT_FILE'"
