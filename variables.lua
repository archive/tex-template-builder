local vars = {}

local additionalChars = {".", ",", "!", ":", ";"}


local function setVariable(meta, key, val)
    local var_key = "$" .. key .. "$"
    vars[var_key] = val
    meta[key] = val
    for _, char in pairs(additionalChars) do
        vars[var_key .. char] = val .. char
        vars[char .. var_key] = char .. val
        meta[var_key .. char] = val .. char
        meta[char .. var_key] = char .. val
    end
end

local function getVarsFromMeta(meta)
    for k, v in pairs(meta) do
        if v.t == 'MetaInlines' then
            setVariable(meta, k, table.unpack(v)["c"])
        end
    end
    return meta
end

return {
    {
        Meta = function (meta)
            setVariable(meta, 'outputFile', os.getenv("OUTPUT_FILE"))
            setVariable(meta, 'inputDir', os.getenv("INPUT_DIR"))
            return getVarsFromMeta(meta)
        end
    },
    {
        Str = function (el)
            if vars[el.text] then
                return pandoc.Span(vars[el.text])
            else
                return el
            end
        end
    }
}
