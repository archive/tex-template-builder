---
title: Tex-Template
date: 2018-01-01
subtitle: Test Document
---
# Hello world

\lipsum[3-10]

Foo is equal to \$foo\$.
Input directory is at \$inputDir\$.

\includegraphics[width=\textwidth]{test-image}

# Test 1
test 1

## Test 1a
test 1a

### Test 1ai
test 1ai

### Test 1aii
test 1aii

#### Test 1aii inner
test 1aii inner

##### Test 1aii inner inner
test 1aii inner inner

### Test 1aiii
test 1aiii

## Test 1b
test 1b

## Test 1c
test 1c

# Test 2
test 2

# Test 3
test 3

# Test 4
test 4


# Test code
```java
public class Test {
    static Set<Thread> updateThreads = new HashSet<Thread>();

    public static void main(String[] args) {
        ConcurrentMap<Integer, String> concurrentMap = new ConcurrentHashMap<Integer, String>();
        for (int i = 0; i < 1000; i++) {
            startUpdateThread(i, concurrentMap);
        }
        for (Map.Entry<Integer, String> entry : concurrentMap.entrySet()) {
            System.out.println("Key :" + entry.getKey() + " Value:" + entry.getValue());
        }
        for (Thread thread : updateThreads) {
            thread.interrupt();
        }
    }
}
```

## Test Inline Code
Some `inline` code.
