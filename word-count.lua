return {
    {
        Meta = function(meta)
            local rawText = pandoc.pipe("pdftotext", {os.getenv("OUTPUT_FILE"), "-"}, "")
            meta["wordcount"] = pandoc.pipe("wc", {"-w"}, rawText)
            return meta
        end,
    },
}
